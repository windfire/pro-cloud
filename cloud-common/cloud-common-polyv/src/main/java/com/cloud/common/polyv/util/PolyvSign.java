package com.cloud.common.polyv.util;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 获取课时code值实体类
 * @author Aijm
 * @since 2019/10/6
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PolyvSign implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课时vid
	 */
	private String vid;

	/**
	 * 播放ip
	 */
	private String ip;

	/**
	 * 用户编号
	 */
	private String userNo;

}
