package com.cloud.common.polyv.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *
 * @author Aijm
 * @since 2019/10/6
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionResult implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 错误码 0为成功 ，其他为失败
	 */
	private String error;

	/**
	 * 问题id
	 */
	private String examId;


}
