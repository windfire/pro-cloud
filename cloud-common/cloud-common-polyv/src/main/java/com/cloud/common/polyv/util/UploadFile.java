package com.cloud.common.polyv.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 上传文件信息
 * @author Aijm
 * @since 2019/10/7
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadFile implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 标签
	 */
	private String tag;

	/**
	 * 描述
	 */
	private String desc;

	private Long cataid;

	private String watermark;


}
