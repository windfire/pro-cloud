package com.cloud.sms.service;

import com.cloud.common.data.base.IProService;
import com.cloud.sms.beans.po.SysMsgConfig;

/**
 * 短信配置信息
 *
 * @author Aijm
 * @date 2019-09-10 13:40:14
 */
public interface SysMsgConfigService extends IProService<SysMsgConfig> {

}
