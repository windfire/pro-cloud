package com.cloud.exam.service;

import com.cloud.common.data.base.IProService;
import com.cloud.exam.beans.po.ExamPaper;

/**
 * 试卷详细信息
 *
 * @author Aijm
 * @date 2019-10-13 23:30:51
 */
public interface ExamPaperService extends IProService<ExamPaper> {

}
