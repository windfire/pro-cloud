package com.cloud.exam.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.exam.beans.po.ExamUserAnswer;
import com.cloud.exam.mapper.ExamUserAnswerMapper;
import com.cloud.exam.service.ExamUserAnswerService;
import org.springframework.stereotype.Service;

/**
 * 测试结果详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:32:31
 */
@Service
public class ExamUserAnswerServiceImpl extends BaseService<ExamUserAnswerMapper, ExamUserAnswer> implements ExamUserAnswerService {

}
