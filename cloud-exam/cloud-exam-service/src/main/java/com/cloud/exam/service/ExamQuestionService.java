package com.cloud.exam.service;

import com.cloud.common.data.base.IProService;
import com.cloud.exam.beans.po.ExamQuestion;

/**
 * 问题详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:00:15
 */
public interface ExamQuestionService extends IProService<ExamQuestion> {

}
