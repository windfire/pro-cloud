package com.cloud.exam.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 问题详细信息
 *
 * @author Aijm
 * @date 2019-10-14 23:14:21
 */
@Data
@TableName("exam_user_paper")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "问题详细信息")
public class ExamUserPaper extends BaseEntity<ExamUserPaper> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "")
    private Long examPaperId;

    @ApiModelProperty(value = "试卷名称")
    private String paperName;

    @ApiModelProperty(value = "分类ID集合(逗号分隔)")
    private String categoryIds;

    @ApiModelProperty(value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(value = "试卷类型( 1固定试卷  2临时试卷 3班级试卷 4.时段试卷 )")
    private Integer paperType;

    @ApiModelProperty(value = "系统判定得分")
    private Integer systemScore;

    @ApiModelProperty(value = "最终得分(千分制)")
    private Integer userScore;

    @ApiModelProperty(value = "试卷总分")
    private Integer paperScore;

    @ApiModelProperty(value = "做对题目数量")
    private Integer questionCorrect;

    @ApiModelProperty(value = "题目总数量")
    private Integer questionCount;

    @ApiModelProperty(value = "做题时间(秒)")
    private Integer doTime;

    @ApiModelProperty(value = "试卷状态(1待判分 2完成)")
    private Integer status;


}
