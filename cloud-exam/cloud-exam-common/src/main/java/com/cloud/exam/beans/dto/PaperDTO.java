package com.cloud.exam.beans.dto;

import com.cloud.exam.beans.po.ExamPaper;
import com.cloud.exam.beans.po.ExamQuestion;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Author Aijm
 * @Description 试卷Dto
 * @Date 2019/11/28
 */
@Data
@Accessors(chain = true)
public class PaperDTO extends ExamPaper {

    /**
     * 所有的 题目集合
     */
    private List<ExamQuestion> examQuestions;
    /**
     * 所有的 题目id集合
     */
    private List<Long> questionIds;
}
