package com.cloud.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author Aijm
 * @Description admin 监控
 * @Date 2019/8/6
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableAdminServer
@ComponentScan({"com.cloud.monitor","com.cloud.common"})
public class AdminMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminMonitorApplication.class, args);
    }
}
