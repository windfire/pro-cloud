# Pro-Cloud

**以下是cloud平台简介**
#### 介绍(如果喜欢的话，请给个star)

Pro-Cloud 不仅仅只是一个单纯的基础框架,他也包含了在线教学、课程管理以及考试等完整功能，让教育机构能够快速高效的建立在线网校，成功转型在线教育。
该项目采用了前后端分离模式，方便个性化页面实现，为APP，小程序、PC端的统一接入管理提供了基石，精准挖掘学员的。
pro-cloud的目标是打造出强大的在线教育系统。基于在线教育平台开发和运营经验打造出来的产品，致力于打造一个全行业都适用的分布式在线教育系统。  

#### 前台主要功能介绍

1. 统一登录中心：打造APP和小程序以及PC端用户的标准化管理，让权限统一化管理分明。 
2. 课程详情：课程介绍、目录的展示以及购买和视频的播放功能等
3. 个人中心: 实现权限化管理用户，对用户的统计，实时了解当前网校发展情况。
4. 通知模块：使用sms 短信，邮件等通知方式，挖掘潜在学员，让消息能够被用户接收。
5. 题目模块：题库管理打造在线教育题库答题系统支持多维度、多角度的数据统计，充分挖掘数据，提供有价值的分析报表，。

#### 相关工程
后台管理前端工程layui（pro-layui）：[码云地址](https://gitee.com/gitsc/pro-layui)  
后台管理前端工程vue-element-admin（pro-ui 待支持）：[码云地址](https://gitee.com/gitsc/pro-ui)  

#### 文档
详细请参考: [pro-cloud技术文档](http://doc.eduvipx.cn)

#### 软件架构
前台采用 vue.js 为核心框架;
后台基于 Spring Cloud alibaba、Spring Security Oauth 2.0 开发企业级认证与授权，提供常见服务监控、链路追踪、日志分析、缓存管理、任务调度等实现，
nacos + Spring Cloud Oauth2 + Spring Cloud gateway +  Feign + mybatisplus等，各种组件注解开发，让代码简洁，通俗易通，以提高效率
```
Pro-Cloud
├── cloud-admin -- 系统基础模块
│   ├── cloud-admin-common  -- auth客户端
│   ├── cloud-admin-api   -- admin暴露的feign接口
│   └── cloud-admin-service -- admin模块的实现
├── cloud-auth  -- auth服务端
├─cloud-common   -- 系统公共模块
│  ├─cloud-common-bom   -- 版本控制
│  ├─cloud-common-cache  -- 缓存工具类
│  ├─cloud-common-controller   -- 全局controller操作工具类
│  ├─cloud-common-data  -- 对数据库操作工具类
│  ├─cloud-common-entity  -- 公共实体工具类
│  ├─cloud-common-job   -- 定时任务工具类
│  ├─cloud-common-mq    -- mq工具类
│  ├─cloud-common-oauth  -- oauth授权工具类
│  ├─cloud-common-oss    -- oss文件上传工具类
│  ├─cloud-common-polyv   -- 保利威视工具类
│  ├─cloud-common-security  -- 客户端安全工具类
│  ├─cloud-common-swagger -- swagger工具类
│  ├─cloud-common-util   -- 基础工具类
│  ├─cloud-common-websocket  -- websocket工具类
│  └─cloud-common-zk   -- zk分布式锁工具类
├─cloud-course   -- 视频课程模块
│  ├─cloud-course-api
│  ├─cloud-course-common
│  └─cloud-course-service
├─cloud-email      -- 发送邮件模块
│  ├─cloud-email-api
│  ├─cloud-email-common
│  └─cloud-email-service
├─cloud-exam   -- 测试模块
│  ├─cloud-exam-api
│  ├─cloud-exam-common
│  └─cloud-exam-service
├─cloud-gateway   -- springcloud gateway 网关 
├─cloud-generator   -- 代码生成
├─cloud-monitor  -- 监控模块
├─cloud-oss  -- oss文件上传模块
│  ├─cloud-oss-api
│  ├─cloud-oss-common
│  └─cloud-oss-service
├─cloud-search  -- es收搜模块
├─cloud-sms   -- 短信模块
│  ├─cloud-sms-api
│  ├─cloud-sms-common
│  └─cloud-sms-service
├─cloud-sso-demo   -- 单点登录案例
├─cloud-transaction   -- 分布式事务
├─cloud-websocket   -- websocket案例
├─cloud-xxl-job   -- xxl-job案例
└── docs    -- pro-cloud文档
```
   
| 版本规划| 解决问题|
|----: |:--------:|
| v0.5 | 微服务架构的搭建，基础数据，用户，角色，部门，多租户，微服务文件上传支持，在线监控等 |
| v0.6 | 定时任务处理xxl-job，分布式事物的解决，代码在线生成器 |
| v0.7 | 保利威视的集成、在线课程以及在线考试的模块完成 |
| v0.8 | vue-element-admin的集成和文档的完善 |
| v0.9 | 课程的评论收藏，以及题目的收藏完成 |
| v1.0 | 在线论坛的完成 |
#### 安装教程

1. 安装mysql redis idea工具
2. 导入代码
3. 使用skywalking 链路追踪

#### 使用说明

1. /auth/oauth/token 获取token 
2. 先启动auth 统一登录中心，然后启动admin模块，统一管理后台
3. /code 获取验证码      
4. 生成代码接口示例：
generator/code?tableName=sys_user&moduleName=admin&comments=用户表     
5. 继承授权中心模块需要实现ProUserDetailsService接口（不实现只会走默认方法），如果定制发邮件需要重构SmsCodeSender接口
6. 继承data模块需要实现SystemService。获取当前用户id（不实现只会走默认方法）
7. 设置是否开启mybatisplus日志拦截性能分析mybatis-plus.performanceInterceptor.enabled:true



#### 参与贡献

1. [Mybatis-Plus](https://mp.baomidou.com/)
2. [Spring Cloud Oauth2](https://spring.io/projects/spring-security-oauth)
3. [Nacos](https://nacos.io/zh-cn/docs/quick-start.html)
4. [hutool](https://www.hutool.cn/docs/#/)


#### Pro-Cloud建设

1. 官方地址 [www.eduvipx.cn](http://www.eduvipx.cn) 文档地址http://www.eduvipx.cn:8000/ 官方网站正在建设中…… 可以先查看文档

![输入图片说明](https://images.gitee.com/uploads/images/2019/1125/204935_02ce4b4b_1236464.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1125/205517_9805bb5a_1236464.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1125/205612_4e340fbe_1236464.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1125/205658_f7fea0ef_1236464.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1125/210037_150c54ce_1236464.png "屏幕截图.png")
