package com.cloud.course.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.TreeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableField;

/**
 * 章节课时信息
 *
 * @author Aijm
 * @date 2019-10-12 22:41:22
 */
@Data
@TableName("course_chapter")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "章节课时信息")
public class CourseChapter extends TreeEntity<CourseChapter> {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "课程ID")
    private Long courseId;


    @ApiModelProperty(value = "是否免费：1免费，0收费")
    @TableField("is_free")
    private Integer hasFree;

    @ApiModelProperty(value = "原价")
    private BigDecimal periodOriginal;

    @ApiModelProperty(value = "优惠价")
    private BigDecimal periodDiscount;

    @ApiModelProperty(value = "购买人数")
    private Integer countBuy;

    @ApiModelProperty(value = "学习人数")
    private Integer countStudy;

    @ApiModelProperty(value = "是否存在文档(1存在，0否)")
    @TableField("is_doc")
    private Integer hasDoc;

    @ApiModelProperty(value = "文档名称")
    private String docName;

    @ApiModelProperty(value = "文档地址")
    private String docUrl;

    @ApiModelProperty(value = "类型(0:视频，1:练习题, 2:目录)")
    private String type;

    @ApiModelProperty(value = "关联资源")
    private Integer resourcesId;

    @ApiModelProperty(value = "状态(1:正常，0:禁用)")
    private Integer statusId;


    @ApiModelProperty(value = "审核状态(0:待审核;1:审核通过;2:审核不通过)")
    private Integer auditStatus;

    @ApiModelProperty(value = "审核意见")
    private String auditOpinion;


}
