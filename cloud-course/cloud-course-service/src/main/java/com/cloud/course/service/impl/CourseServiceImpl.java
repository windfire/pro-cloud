package com.cloud.course.service.impl;

import com.cloud.common.data.base.BaseService;
import com.cloud.course.beans.po.Course;
import com.cloud.course.mapper.CourseMapper;
import com.cloud.course.service.CourseService;
import org.springframework.stereotype.Service;

/**
 * 课程信息
 *
 * @author Aijm
 * @date 2019-10-10 22:02:15
 */
@Service
public class CourseServiceImpl extends BaseService<CourseMapper, Course> implements CourseService {

}
