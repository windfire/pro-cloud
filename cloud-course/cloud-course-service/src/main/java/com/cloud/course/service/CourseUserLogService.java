package com.cloud.course.service;

import com.cloud.common.data.base.IProService;
import com.cloud.course.beans.po.CourseUserLog;

/**
 * 课程用户学习详细信息日志
 *
 * @author Aijm
 * @date 2019-10-13 16:46:28
 */
public interface CourseUserLogService extends IProService<CourseUserLog> {

}
