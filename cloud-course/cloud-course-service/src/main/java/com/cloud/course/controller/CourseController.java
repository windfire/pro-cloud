package com.cloud.course.controller;

import com.cloud.common.util.base.Result;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.course.beans.po.Course;
import com.cloud.course.service.CourseService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 课程信息
 *
 * @author Aijm
 * @date 2019-10-10 22:02:15
 */
@RestController
@RequestMapping("/course" )
@Api(value = "course", tags = "course管理")
public class CourseController {

    @Autowired
    private CourseService courseService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param course 课程信息
     * @return
     */
    @GetMapping("/page")
    public Result getCoursePage(Page page, Course course) {
        return Result.success(courseService.page(page, Wrappers.query(course)));
    }


    /**
     * 通过id查询课程信息
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    public Result getById(@PathVariable("id") Long id) {
        return Result.success(courseService.getById(id));
    }

    /**
     * 新增课程信息
     * @param course 课程信息
     * @return Result
     */
    @PostMapping
    @PreAuthorize("@pms.hasPermission('course_course_add')")
    public Result save(@RequestBody @Valid Course course) {
        return Result.success(courseService.save(course));
    }

    /**
     * 修改课程信息
     * @param course 课程信息
     * @return Result
     */
    @PutMapping
    @PreAuthorize("@pms.hasPermission('course_course_edit')")
    public Result updateById(@RequestBody @Valid Course course) {
        return Result.success(courseService.updateById(course));
    }

    /**
     * 通过id删除课程信息
     * @param id id
     * @return Result
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("@pms.hasPermission('course_course_del')")
    public Result removeById(@PathVariable Long id) {
        return Result.success(courseService.removeById(id));
    }

}
