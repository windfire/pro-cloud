package com.cloud.course.mapper;

import com.cloud.common.data.base.ProMapper;
import com.cloud.course.beans.po.CourseUserLog;

/**
 * 课程用户学习详细信息日志
 *
 * @author Aijm
 * @date 2019-10-13 16:46:28
 */
public interface CourseUserLogMapper extends ProMapper<CourseUserLog> {

}
