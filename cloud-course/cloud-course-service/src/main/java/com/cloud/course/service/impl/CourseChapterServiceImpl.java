package com.cloud.course.service.impl;

import com.cloud.common.data.base.TreeService;
import com.cloud.course.beans.po.CourseChapter;
import com.cloud.course.mapper.CourseChapterMapper;
import com.cloud.course.service.CourseChapterService;
import org.springframework.stereotype.Service;

/**
 * 章节课时信息
 *
 * @author Aijm
 * @date 2019-10-12 22:41:22
 */
@Service
public class CourseChapterServiceImpl extends TreeService<CourseChapterMapper, CourseChapter> implements CourseChapterService {

}
